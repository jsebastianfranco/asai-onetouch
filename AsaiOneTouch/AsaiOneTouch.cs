﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsaiOneTouch
{
    public class AsaiOneTouch
    {
        public int timer;
        public string descriptionAsaiOneTouch
        {
            get;
            set;
        }


        private void ReadFilePaths()
        {
            //string files = string.Empty;
            //string paths = string.Empty;
            //timer = 1;

            //Show the files and folders to process

           /* int copyCount = 8;*//*Convert.ToInt32(ConfigurationManager.AppSettings["TouchCount"]);*/


            //for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            //{
            //    string app = "Touch" + rowCounter;
            //    //files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app]) + "\n";
            //    paths = (System.IO.Path.Combine(ConfigurationManager.AppSettings[app])).Trim();
            //    timer += 1;

                //call copy function and send the paths

            //1
                if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 1))
                {
                    Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 1);
                }

                CopyFiles(@"C:\ProFlex4\Base\Journals", @"C:\ASAI\OneTouchPF4\OneTouch\" + 1);

            //2
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 2))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 2);
            }

            CopyFiles(@"C:\ASAI\CacheConfigFile", @"C:\ASAI\OneTouchPF4\OneTouch\" + 2);

            //3
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 3))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 3);
            }

            CopyFiles(@"C:\ASAI\PF4Traces", @"C:\ASAI\OneTouchPF4\OneTouch\" + 3);

            //4
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 4))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 4);
            }

            CopyFiles(@"C:\ASAI\LogFiles", @"C:\ASAI\OneTouchPF4\OneTouch\" + 4);

            //5
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 5))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 5);
            }

            CopyFiles(@"C:\ASAI\LogData", @"C:\ASAI\OneTouchPF4\OneTouch\" + 5);

            //6
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 6))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 6);
            }

            CopyFiles(@"C:\ASAI\Traces", @"C:\ASAI\OneTouchPF4\OneTouch\" + 6);

            //7
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 7))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 7);
            }

            CopyFiles(@"C:\ASAI\TicketRedemption\Logs", @"C:\ASAI\OneTouchPF4\OneTouch\" + 7);

            //8
            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\" + 8))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + 8);
            }

            CopyFiles(@"C:\ASAI\counters", @"C:\ASAI\OneTouchPF4\OneTouch\" + 8);

            //}


            Thread.Sleep(1000);

            //timer += 1;


        }

        public static void CopyFiles(string sourceDirectory, string destinationPath)
        {
            DirectoryInfo dirSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo dirDestination = new DirectoryInfo(destinationPath);

            CopyAllFiles(dirSource, dirDestination);
        }


        public static void CopyAllFiles(DirectoryInfo source, DirectoryInfo destination)
        {

            try
            {
                foreach (FileInfo fi in source.GetFiles())
                {
                    wLogging.log(@"Copying the file {0}\{1}" + destination.FullName + fi.Name);
                    fi.CopyTo(Path.Combine(destination.FullName, fi.Name), true);
                }

                // Copy each subdirectory using recursion.
                foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
                {
                    DirectoryInfo nextTargetSubDir = destination.CreateSubdirectory(diSourceSubDir.Name);
                    CopyAllFiles(diSourceSubDir, nextTargetSubDir);
                }
            }
            catch (Exception err)
            {
                wLogging.log("Error Trying copy the source files into destination folder : " + err);
            }


        }

        public void CompressFolder()
        {
            try
            {
                string startPath = @"C:\ASAI\OneTouchPF4\OneTouch";
                string zipPath = @"C:\ASAI\OneTouchPF4\OneTouch" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".zip";

                System.IO.Compression.ZipFile.CreateFromDirectory(startPath, zipPath);
            }
            catch (Exception err)
            {
                wLogging.log("Error Trying compress the source files into destination folder : " + err);
            }

        }

        public void deleteTouchFolder()
        {
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(@"C:\ASAI\OneTouchPF4\OneTouch");

                if (Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch"))
                {
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }
            }
            catch (Exception err)
            {

                wLogging.log("Error Trying delete the source folder : " + err);
            }

        }

        public int CreateOneT()
        {

            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch"))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch");
            }

            try
            {
                ReadFilePaths();
                Thread.Sleep(1000);


                CompressFolder();
                Thread.Sleep(1000);


                Thread.Sleep(1000);
                deleteTouchFolder();

                descriptionAsaiOneTouch = "File created successfully";
                return 1;
            }
            catch (Exception err)
            {
                descriptionAsaiOneTouch = "Failed to created one touch" + err.Message;
                return 0;
            }
        }

    }
}
